from pprint import pprint

from gensim.corpora import Dictionary
from gensim.models.wrappers import LdaMallet

import all_files
from lines_writer_reader import read_document
from txtWorker import TxtWorker

path_to_mallet_binary = "Mallet/bin/mallet"


def lda_model(document, num_topics, show_words):
    my_dictionary = Dictionary(document)
    my_corpus = [my_dictionary.doc2bow(text) for text in document]
    model = LdaMallet(path_to_mallet_binary, corpus=my_corpus, num_topics=num_topics, id2word=my_dictionary)
    results = []
    for i in range(num_topics):
        results.append(sorted(model.show_topic(i, topn=show_words), key=lambda x: -x[1]))
    return results


def start_lda_model():
    filename = all_files.filtered_document
    out_filename = all_files.result_name
    topics_list = [20, 30, 50]
    show_words_num = 20
    lines = read_document(filename)
    for topic_num in topics_list:
        result = lda_model(lines, topic_num, show_words_num)
        TxtWorker.write(out_filename.format(topic_num), str(result))


if __name__ == '__main__':
    start_lda_model()

