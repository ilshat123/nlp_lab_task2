import stopwordsiso as stopwords

import all_files
from lines_writer_reader import read_document, write_document
from txtWorker import TxtWorker


def del_bad_words(words_dict, delete_words):
    for word in delete_words:
        try:
            del words_dict[word]
        except KeyError:
            pass
    return words_dict


def create_words_dict(words):
    result = {}
    for word in words:
        res = result.get(word, 0) + 1
        result[word] = res
    return result


def filter_doc():
    filename = all_files.document
    result_filename = all_files.filtered_document
    lines = read_document(filename)
    all_words = []
    for line in lines:
        all_words.extend(line)

    words_dict = create_words_dict(all_words)
    bad_words = [word for word in words_dict if words_dict[word] < 5]
    bad_words.extend(stopwords.stopwords("ru"))
    words_dict = del_bad_words(words_dict, bad_words)

    result_file = []
    for line in lines:
        new_line = [word for word in line if word in words_dict]
        result_file.append(new_line)
    write_document(result_filename, result_file)


if __name__ == '__main__':
    filter_doc()




