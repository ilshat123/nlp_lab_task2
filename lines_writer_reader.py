from txtWorker import TxtWorker


#  При записи через json, файл разрастается до 900 мб, а так остаются те же 300.
#  Можно было сделать еще и через pandas.to_csv, но так удобнее
#  Чтение/запись все равно происходит через это место, поэтому дальше дело только в реализации

def read_document(filename):
    """

    :param filename:
    :return: [[elem1, elem2 ...], ...]
    """
    lines = TxtWorker.read(filename).split('\n')
    lines = [line.split('\t') for line in lines]
    return lines


def write_document(filename, document):
    """

    :param filename:
    :param document: [[elem1, elem2 ...], ...]
    :return:
    """
    lines = '\n'.join(['\t'.join(line) for line in document])
    TxtWorker.write(filename, lines)
