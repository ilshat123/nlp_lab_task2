from pprint import pprint
import pandas as pd

import all_files
from txtWorker import TxtWorker
from document_filter import create_words_dict


def write_words_num(filtered_document, output_filename):
    filename = filtered_document
    file = TxtWorker.read(filename).split('\n')
    file = [line.split('\t') for line in file]

    words = []
    for line in file:
        words.extend(line)

    words_dict = create_words_dict(words)
    words_dict = sorted(words_dict.items(), key=lambda x: -x[1])

    df = pd.DataFrame({'word': [elem[0] for elem in words_dict],
                       'num': [elem[1] for elem in words_dict]})
    csv_file = df.to_csv(index=False)
    TxtWorker.write(output_filename, csv_file)


def create_topic_words(input_filename, output_filename):
    file = eval(TxtWorker.read(input_filename))
    result_lines = []
    for elems in file:
        result_lines.append(' '.join([elem[0] for elem in elems]))
    TxtWorker.write(output_filename, '\n'.join(result_lines))


def create_topics_res(input_files, output_files):
    for i in range(len(input_files)):
        create_topic_words(input_files[i], output_files[i])


def create_results():
    topics = [20, 30, 50]
    files = [all_files.result_name.format(topic) for topic in topics]
    output = [name.replace(all_files.work_folder, all_files.results_folder) for name in files]

    write_words_num(all_files.filtered_document, 'results/words_num.csv')
    create_topics_res(files, output)


if __name__ == '__main__':
    create_results()

