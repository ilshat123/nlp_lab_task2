from create_normal_form import create_normal_form
from create_results import create_results
from document_filter import filter_doc
from lda_mallet import start_lda_model
from py_lda_vis import show_py_lda_vis

if __name__ == '__main__':
    create_normal_form()
    filter_doc()
    start_lda_model()
    create_results()
    show_py_lda_vis()
