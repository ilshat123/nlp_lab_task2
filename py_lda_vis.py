import pyLDAvis.gensim as gensimvis
import pyLDAvis
from gensim.corpora import Dictionary
from gensim.models import LdaModel

import all_files
from lines_writer_reader import read_document
from txtWorker import TxtWorker


def show_py_lda_vis():
    filename = all_files.filtered_document
    num_topics = 50
    lines = read_document(filename)
    my_dictionary = Dictionary(lines)
    my_corpus = [my_dictionary.doc2bow(text) for text in lines]
    model = LdaModel(corpus=my_corpus, num_topics=num_topics, id2word=my_dictionary)

    vis_data = gensimvis.prepare(model, my_corpus, my_dictionary)
    res = pyLDAvis.display(vis_data)
    pyLDAvis.show(vis_data)


if __name__ == '__main__':
    show_py_lda_vis()

