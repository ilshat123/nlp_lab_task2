import json
import re
import sys
import pandas
import pymorphy2

import all_files
from lines_writer_reader import write_document
from txtWorker import TxtWorker

morph = pymorphy2.MorphAnalyzer()


def get_words_dict(iterable_text_obj):
    document = []
    for i in range(len(iterable_text_obj)):
        line = iterable_text_obj[i]
        line = re.sub(r'@[^\s]+', '', line)
        # print(line)
        words = re.findall(r'\w+', line)
        words = [morph.parse(word)[0].normal_form for word in words
                 if not word.isnumeric()]
        document.append(words)
        print(i)
    return document


def create_normal_form():
    filename = all_files.tweets
    result_file = all_files.document
    data = pandas.read_csv(filename, compression='gzip')
    words = data['text']
    document = get_words_dict(words)
    write_document(result_file, document)


if __name__ == '__main__':
    create_normal_form()


